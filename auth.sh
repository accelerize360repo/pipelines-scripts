#!/bin/bash
if [ $1 == 'Sandbox' -o $1 == '-s' ]
then
    URL="https://test.salesforce.com"
else
    URL="https://login.salesforce.com"
fi
sfdx force:auth:jwt:grant --instanceurl $URL --clientid $2 --jwtkeyfile pipelines-scripts/assets/server.key --username $3 --setalias TargetOrg