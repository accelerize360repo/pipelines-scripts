#!/bin/bash
# Create branch
if [ $1 == 'Manifest' -o $1 == '-m' ]
then
    PKG_NAME_TRIMMED=${2// /-}
    BRANCH_NAME="sync/$PKG_NAME_TRIMMED"
    COMMIT_MSG="Sync - Package $2 [skip ci]"
else
    BRANCH_NAME="sync/all"
    COMMIT_MSG="Sync - All [skip ci]"
fi
git checkout -b $BRANCH_NAME

# If DEPLOY_MODE=Manifest use Change Set to construct manifest and retrieve metadata
# If DEPLOY_MODE=Diff retrieve all metadata
if [ $1 == 'Manifest' -o $1 == '-m' ]
then
    # Retrieve Metadata API Source from Change Set (or Package)
    sfdx force:mdapi:retrieve -s -r ./mdapipkg -u TargetOrg -p "$2"
    # Unzip the file
    unzip -o -qq ./mdapipkg/unpackaged.zip -d ./mdapipkg
    # Copy package.xml to manifest directory
    yes | cp -rfa ./mdapipkg/package.xml ./manifest/
    # Delete the mdapipkg folder
    rm -rf ./mdapipkg

    sfdx force:source:retrieve -u TargetOrg -x manifest/package.xml
else
    # Some metadata can result in an error depending on org features, so retrieve them separately
    sfdx force:source:retrieve -u TargetOrg -m AccessControlPolicy,AccountForecastSettings,AccountInsightsSettings,AccountIntelligenceSettings,AccountSettings,AcctMgrTargetSettings,ActionLinkGroupTemplate,ActionPlanTemplate,ActionsSettings,ActivitiesSettings,AddressSettings,AIAssistantTemplate,AIReplyRecommendationsSettings,AnalyticSnapshot,AnalyticsSettings,AnimationRule,ApexClass,ApexComponent,ApexPage,ApexSettings,ApexTestSuite,ApexTrigger,AppExperienceSettings,AppMenu,AppointmentSchedulingPolicy,ApprovalProcess,ArchiveSettings,AssignmentRules,AssistantContextItem,AssistantDefinition,AssistantSkillQuickAction,AssistantSkillSobjectAction,AssistantVersion,AuraDefinitionBundle,AuthProvider,AutomatedContactsSettings,AutoResponseRules,BlockchainSettings,Bot,BotSettings,BotVersion,BrandingSet,BusinessHoursSettings,BusinessProcess,CallCenter,CampaignInfluenceModel,CampaignSettings,CanvasMetadata,CareProviderSearchConfig,CaseClassificationSettings,CaseSettings,CaseSubjectParticle,Certificate,ChannelLayout,ChatterAnswersSettings,ChatterEmailsMDSettings,ChatterExtension,ChatterSettings,CleanDataService,CMSConnectSource,CommandAction,CommunitiesSettings,Community,CommunityTemplateDefinition,CommunityThemeDefinition,CompactLayout,CompanySettings,ConnectedApp,ConnectedAppSettings,ContentAsset,ContentSettings,ContractSettings,CorsWhitelistOrigin,CspTrustedSite,CurrencySettings,CustomApplication,CustomApplicationComponent,CustomerDataPlatformSettings,CustomFeedFilter,CustomField,CustomHelpMenuSection,CustomLabels,CustomMetadata,CustomNotificationType,CustomObject,CustomObjectTranslation,CustomPageWebLink,CustomPermission,CustomSite,CustomTab,Dashboard,DashboardFolder,DataCategoryGroup,DataDotComSettings,DelegateGroup,DeploymentSettings,DevHubSettings,DiscoverySettings,Document,DocumentChecklistSettings,DocumentFolder,DocumentType,DuplicateRule,EACSettings,EclairGeoData,EinsteinAssistantSettings,EmailAdministrationSettings,EmailFolder,EmailIntegrationSettings,EmailServicesFunction,EmailTemplate,EmailTemplateSettings,EmbeddedServiceBranding,EmbeddedServiceConfig,EmbeddedServiceFlowConfig,EmbeddedServiceLiveAgent,EncryptionKeySettings,EnhancedNotesSettings,EntitlementProcess,EntitlementSettings,EntitlementTemplate,EntityImplements,EscalationRules,EssentialsSettings,EventSettings,ExperienceBundle,ExperienceBundleSettings,ExternalDataSource,ExternalServiceRegistration,ExternalServicesSettings,FeatureParameterBoolean,FeatureParameterDate,FeatureParameterInteger,FieldServiceSettings,FieldSet,FilesConnectSettings,FileUploadAndDownloadSecuritySettings,FlexiPage,Flow,FlowCategory,FlowDefinition,FlowSettings,ForecastingSettings,Form,FormSection,FormulaSettings,GlobalValueSet,GlobalValueSetTranslation,GoogleAppsSettings,Group,HighVelocitySalesSettings,HomePageComponent,HomePageLayout,Icon,IdeasSettings,Index,IndustriesManufacturingSettings,IndustriesSettings,InstalledPackage,InvocableActionSettings,IoTSettings,IsvHammerSettings,KeywordList,KnowledgeSettings,LanguageSettings,Layout,LeadConfigSettings,LeadConvertSettings,Letterhead,LightningBolt,LightningComponentBundle,LightningExperienceSettings,LightningExperienceTheme,LightningMessageChannel,LightningOnboardingConfig,ListView,LiveAgentSettings,LiveChatAgentConfig,LiveChatButton,LiveChatDeployment,LiveChatSensitiveDataRule,LiveMessageSettings,MacroSettings,ManagedTopics,MapsAndLocationSettings,MatchingRules,MilestoneType,MlDomain,MobileApplicationDetail,MobileSettings,ModerationRule,MutingPermissionSet,MyDomainDiscoverableLogin,MyDomainSettings,NamedCredential,NameSettings,NavigationMenu,Network,NetworkBranding,NotificationsSettings,OauthCustomScope,ObjectLinkingSettings,OmniChannelSettings,OpportunityInsightsSettings,OpportunitySettings,Orchestration,OrchestrationContext,OrderManagementSettings,OrderSettings,OrgSettings,PardotEinsteinSettings,PardotSettings,PartyDataModelSettings,PathAssistant,PathAssistantSettings,PaymentGatewayProvider,PermissionSet,PermissionSetGroup,PicklistSettings,PlatformCachePartition,PlatformEncryptionSettings,PlatformEventChannel,PlatformEventChannelMember,Portal,PortalsSettings,PostTemplate,PredictionBuilderSettings,PresenceDeclineReason,PresenceUserConfig,PrivacySettings,PrivateConnection,ProductSettings,Profile,ProfilePasswordPolicy,ProfileSessionSetting,Prompt,Queue,QueueRoutingConfig,QuickAction,QuickTextSettings,QuoteSettings,RecommendationStrategy,RecordActionDeployment,RecordPageSettings,RecordType,RedirectWhitelistUrl,RemoteSiteSetting,Report,ReportFolder,ReportType,RestrictionRule,RetailExecutionSettings,Role,SalesAgreementSettings,SamlSsoConfig,SchemaSettings,Scontrol,SearchSettings,SecuritySettings,ServiceChannel,ServicePresenceStatus,SharingCriteriaRule,SharingGuestRule,SharingOwnerRule,SharingReason,SharingRules,SharingSet,SharingSettings,SharingTerritoryRule,SiteDotCom,SiteSettings,Skill,SocialCustomerServiceSettings,SocialProfileSettings,StandardValue,StandardValueSet,StandardValueSetTranslation,StaticResource,SurveySettings,SynonymDictionary,SystemNotificationSettings,Territory,Territory2,Territory2Model,Territory2Rule,Territory2Settings,Territory2Type,TimeSheetTemplate,TopicsForObjects,TrailheadSettings,TransactionSecurityPolicy,Translations,TrialOrgSettings,UIObjectRelationConfig,UiPlugin,UserCriteria,UserEngagementSettings,UserInterfaceSettings,UserManagementSettings,ValidationRule,WaveApplication,WaveDashboard,WaveDataflow,WaveDataset,WaveLens,WaveRecipe,WaveTemplateBundle,WaveXmd,WebLink,WebToXSettings,WorkDotComSettings,Workflow,WorkflowAlert,WorkflowFieldUpdate,WorkflowFlowAction,WorkflowKnowledgePublish,WorkflowOutboundMessage,WorkflowRule,WorkflowSend,WorkflowTask,WorkSkillRouting
    sfdx force:source:retrieve -u TargetOrg -m AccountRelationshipShareRule
fi

# Delete temp folders
rm -rf ./sfdx
rm -rf ./pipelines-scripts

# Push changes to repo (skip Pipelines)
git add -A
git commit -m "$COMMIT_MSG"
git push -u origin $BRANCH_NAME --force

# If AutoMerge = yes merge into $BITBUCKET_BRANCH
if [ $3 == 'Y' -o $3 == 'Yes' -o $3 == 'yes' -o $3 == 'y' -o $3 == 'true' ]
then
    git merge $BRANCH_NAME
fi