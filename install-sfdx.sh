#!/bin/bash
CLIURL="https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz"
# Create sfdx directory
mkdir sfdx
# Install Salesforce CLI
wget -qO- $CLIURL | tar xJ -C sfdx --strip-components 1
"./sfdx/install"
PATH="./sfdx/$(pwd):$PATH"
sfdx --version
sfdx plugins --core